FROM node:16.15.0

# Create app directory
WORKDIR /app

# Install app dependencies
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install
COPY . .

# If you are building your code for production
RUN cp .env.docker .env

EXPOSE 3000
CMD [ "npm", "start" ]