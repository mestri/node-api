# node-api

**Context:** We would like you to build a small API to test your knowledge of Back End Development and related technologies.
The server, once an hour, should connect to the API (refer to the below url) which shows recently posted articles about Node.js on Hacker News. It should insert the data from the API into a database and also define a REST API which the client (e.g. Postman) will be used to retrieve the data.
Hacker News URL: https://hn.algolia.com/api/v1/search_by_date?query=nodejs (External API)
The service should return paginated results with a maximum of 5 items and should be able to be filtered by author, \_tags, title. Also, this should permit the user to remove items and these ones should not reappear when the app is restarted.

## node-api architecture

![node-api architecture](./architecture.png)

## Stack

- nodeJS v16.5.0
- Express v4.18.1
- MongoDB v5
- Docker v20.10.14

## Initial setup

1. ### install dependencies

   - nodemon => Runner dev
   - axios => Client REST
   - dotenv => env files interpreter
   - express => Micro framework for APIs
   - mongoose => Database mapper
   - node-schedule => Job scheduler
   - jest => Testing library
   - eslint => Linter app

All dependencies will be installed using the following command:

```
npm install
```

2. ### Environment setup

For run the app copy .env.default to .env file:

```
cp .env.default .env
```

## Running the apps

1.  ### Running scheduler app

The scheduler app connects to the external API once an hour and saves the data in MongoDB. It runs using the following command:

```
npm run start-scheduler
```

2. ### Running node API

The node API exposes the data saved into MongoDB. It runs using the following command:

```
npm start
```

## Using the API

1. ### Request news

Use the url http://localhost:3000/news

To request a personalized paginated result use the "offset" and "limit" query params:

- Offset: Defines the initial position of the requested data, the default value is 0, which means that the result will be initialized in the first item or first news in the database.

- Limit: Defines the maximum limit of returned items, the default value is 5, which means that the result will be paginated with a maximum of 5 items.

An example of a URL will be:

http://localhost:3000/news?offset=0&limit=5

2. ### Filter a request

It can be filtered using the "author", "title", and "\_tags" query params.

An example of a URL will be:

http://localhost:3000/news?offset=0&limit=5&author=Blahah&title=Mytitle&\_tags=["comment","author_cultofmetatron","story_31470482"]

In the case of the "\_tag" query param, all the tags in the array will be matched. In the previous example ["comment","author_cultofmetatron","story_31470482"] all of the three tags will match to obtain the requested data.

3. ### Remove Items

It removes an item by id, the id should be sent in the URL, for example:

http://localhost:3000/news/628abe91559283b51f671ef1

## Tests

1. ### Testing apps

The tests were created with Jest, for run the tests use the following command:

```
npm test
```

2. ### Testing coverage

To measure the test coverage uses the following command:

```
npm run test:coverage
```

![test coverage on Gitlab](./test-coverage.png)

## Bonus

1. ### Postman collection

All the postman collection is in the project:

[Postman collection](./Challenge.postman_collection.json)

![Postman request](./postman.png)

2. ### Apps Dockerized

- Install docker
- Install docker-compose
- Copy .env.docker to .env
- Run docker-compose

```
docker-compose up
```

Docker Files:

> - Dockerfile
> - docker-compose.yaml

3. ### Linter setup

The .eslint file was created to define the rules for linter the apps.

4. ### Tests and linters on GitLab pipeline (gitlab-ci.yml)

The gitlab-ci.yml file was created to set the pipelines running in Gitlab.

![gitlab pipelines](./gitlab-pipelines.png)
