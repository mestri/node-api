const getFilters = (query) => {
  const filters = {};

  for (const property in query) {
    if (property === '_tags') {
      query[property] = JSON.parse(query[property]);
    }

    if (property !== 'offset' && property !== 'limit') {
      filters[property] = query[property];
    }
  }
  //console.log('filters', filters);
  return filters;
};

module.exports = getFilters;
