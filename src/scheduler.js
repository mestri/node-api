/* eslint-disable no-unused-vars */
const nodeSchedule = require('node-schedule');
const schedulerJob = require('./schedulerJob');
require('dotenv').config();
require('./dbConnection');

const job = nodeSchedule.scheduleJob(
  //'0 * * * *',
  '5 * * * *',
  schedulerJob
);
