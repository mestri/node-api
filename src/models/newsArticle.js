const mongoose = require('mongoose');

const newsSchema = new mongoose.Schema({
  created_at: {
    type: String //,
    //required: true,
    //lowercase: true
  },
  title: {
    type: String
  },
  url: {
    type: String
  },
  author: {
    type: String
  },
  points: {
    type: String
  },
  story_text: {
    type: String
  },
  comment_text: {
    type: String
  },
  num_comments: {
    type: Number
  },
  story_id: {
    type: String
  },
  story_title: {
    type: String
  },
  story_url: {
    type: String
  },
  parent_id: {
    type: String
  },
  created_at_i: {
    type: String
  },
  _tags: {
    type: [String]
  }
});

const NewsArticle = mongoose.model('News', newsSchema);

module.exports = NewsArticle;
