const axios = require('axios');
const NewsArticle = require('./models/newsArticle');

function schedulerJob() {
  // console.log('Job has been triggered at: ');
  axios
    .get(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs'
    )
    .then((res) => {
      //console.log(`Status: ${res.status}`);
      //console.log('Body: ', res.data);
      res.data.hits.forEach((hint) => {
        const newsArticle = new NewsArticle({
          created_at: hint.created_at,
          title: hint.title,
          url: hint.url,
          author: hint.author,
          points: hint.points,
          story_text: hint.story_text,
          comment_text: hint.comment_text,
          num_comments: hint.num_comments,
          story_id: hint.story_id,
          story_title: hint.story_title,
          story_url: hint.story_url,
          parent_id: hint.parent_id,
          created_at_i: hint.created_at_i,
          _tags: hint._tags
        });
        newsArticle.save(function (err, doc) {
          console.log(doc._id);
        });
      });
    })
    .catch((err) => {
      console.error(err);
    });
}

module.exports = schedulerJob;
