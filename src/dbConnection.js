const mongoose = require('mongoose');
const server = process.env.DB_SERVER ?? 'localhost';

mongoose
  .connect(`mongodb://${server}:27017/node-api-db`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log(`CONNECTED TO MONGO!`);
  })
  .catch((err) => {
    console.log(`OH NO! MONGO CONNECTION ERROR!`);
    console.log(err);
  });
