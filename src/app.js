const express = require('express');
const app = express();
const port = 3000;
const NewsArticle = require('./models/newsArticle');
const getFilters = require('./getFilters');
require('dotenv').config();
require('./dbConnection');

delete('/news/:id', (req, res) => {
  const { id } = req.params;
  NewsArticle.findByIdAndDelete(id, function (err) {
    if (err) console.log(err);
    res.send('Successful deletion');
  });
});

app.get('/news', (req, res) => {
  //console.log('req', req);
  NewsArticle.find(getFilters(req.query))
    .skip(req.query.offset ?? 0)
    .limit(req.query.limit ?? 5) //
    .then((data) => {
      //console.log(data);
      res.send(data);
    })
    .catch((e) => {
      console.log('error', e);
    });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
