const getFilters = require('../src/getFilters');

describe('getFilters', () => {
  it('Should ignore offset and limit', () => {
    const sampleQuery = {
      offset: 10,
      limit: 5,
      title: 'My title'
    };

    const queryResult = getFilters(sampleQuery);
    expect(queryResult).toEqual({ title: 'My title' });
  });
  it('Should process tags', () => {
    const sampleQuery = {
      offset: 10,
      limit: 5,
      title: 'My title',
      _tags: '["comment","author"]'
    };

    const queryResult = getFilters(sampleQuery);
    expect(queryResult).toEqual({
      title: 'My title',
      _tags: ['comment', 'author']
    });
  });
});
