const schedulerJob = require('../src/schedulerJob');
const axios = require('axios');
const NewsArticle = require('../src/models/newsArticle');

jest.mock('axios');
jest.mock('../src/models/newsArticle');

describe('schedulerJob', () => {
  it('should run an axios request', async() => {
    axios.get.mockResolvedValue({
      data: {
        hits: [
          {
            created_at: '2022-05-22T20:49:43.000Z',
            title: null,
            url: null,
            author: 'voldacar',
            points: null,
            story_text: null,
            comment_text:
              'There is only so much computation you can do per watt on current process nodes. To increase our computation per chip, which is the goal, we need to increase the amount of watts we consume per chip. The goal should be to make more powerful processors, not ones that do the same with less power.',
            num_comments: null,
            story_id: 31448792,
            story_title:
              'Intel plans immersion lab to chill its power-hungry chips',
            story_url:
              'https://www.theregister.com/2022/05/19/intel_immersion_lab/',
            parent_id: 31472156,
            created_at_i: 1653252583,
            _tags: [
              'comment',
              'author_voldacar',
              'story_31448792'
            ],
            objectID: '31472627',
            _highlightResult: {
              author: {
                value: 'voldacar',
                matchLevel: 'none',
                matchedWords: []
              },
              comment_text: {
                value:
                  'There is only so much computation you can do per watt on current process <em>nodes</em>. To increase our computation per chip, which is the goal, we need to increase the amount of watts we consume per chip. The goal should be to make more powerful processors, not ones that do the same with less power.',
                matchLevel: 'full',
                fullyHighlighted: false,
                matchedWords: ['nodejs']
              },
              story_title: {
                value:
                  'Intel plans immersion lab to chill its power-hungry chips',
                matchLevel: 'none',
                matchedWords: []
              },
              story_url: {
                value:
                  'https://www.theregister.com/2022/05/19/intel_immersion_lab/',
                matchLevel: 'none',
                matchedWords: []
              }
            }
          },
          {
            created_at: '2022-05-22T19:39:25.000Z',
            title: null,
            url: null,
            author: 'cultofmetatron',
            points: null,
            story_text: null,
            comment_text:
              'blender is a mesh modeler, That means the underlying represntation of the scene is a a set of surfaces connected by nodes subdivided by triangles. Its efficient and quick for 3d animation and concept art but not so great for cad design.<p>real cad tools like fusion360, autocad etc are parametric. they represent a shape in the form of constraints. The underlying representation of the shape is different in a way that facilitates modeling something that you are going to create a real world counterpart from.<p>Yes there are open source cad programs but nothing I&#x27;ve seen comes close to rivaling professional tools the way blender does.',
            num_comments: null,
            story_id: 31470482,
            story_title:
              'Constraint-based geometry (CAD) sketcher for Blender',
            story_url:
              'https://github.com/hlorus/CAD_Sketcher',
            parent_id: 31471897,
            created_at_i: 1653248365,
            _tags: [
              'comment',
              'author_cultofmetatron',
              'story_31470482'
            ],
            objectID: '31471927',
            _highlightResult: {
              author: {
                value: 'cultofmetatron',
                matchLevel: 'none',
                matchedWords: []
              },
              comment_text: {
                value:
                  "blender is a mesh modeler, That means the underlying represntation of the scene is a a set of surfaces connected by <em>nodes</em> subdivided by triangles. Its efficient and quick for 3d animation and concept art but not so great for cad design.<p>real cad tools like fusion360, autocad etc are parametric. they represent a shape in the form of constraints. The underlying representation of the shape is different in a way that facilitates modeling something that you are going to create a real world counterpart from.<p>Yes there are open source cad programs but nothing I've seen comes close to rivaling professional tools the way blender does.",
                matchLevel: 'full',
                fullyHighlighted: false,
                matchedWords: ['nodejs']
              },
              story_title: {
                value:
                  'Constraint-based geometry (CAD) sketcher for Blender',
                matchLevel: 'none',
                matchedWords: []
              },
              story_url: {
                value:
                  'https://github.com/hlorus/CAD_Sketcher',
                matchLevel: 'none',
                matchedWords: []
              }
            }
          }
        ]
      }
    });
    const response = await schedulerJob();
    expect(response).toEqual(undefined);
    expect(NewsArticle).toHaveBeenCalledTimes(2);
  });
});
